<?php

/**
 * @file
 * Base class for migrating privatemsg entities.
 *
 * @todo Handle deletions
 */

abstract class DrupalPrivatemsg5Migration extends DrupalMigration {

  /**
   * Constructor.
   */
  public function __construct(array $arguments) {
    if (!empty($arguments['user_migration'])) {
      $user_migration = $arguments['user_migration'];
      $this->dependencies[] = $user_migration;
    }
    parent::__construct($arguments);

    $query = $this->query();
    $this->sourceFields = array();
    $this->sourceFields['id'] = t('Unique ID for the private message');
    $this->sourceFields['author'] = t('The UID of the author');
    $this->sourceFields['recipient'] = t('The UID(s) of the recipients');
    $this->sourceFields['subject'] = t('The subject of the message');
    $this->sourceFields['message'] = t('The message body text');
    $this->sourceFields['timestamp'] = t('The time at which the mesage was sent');
    $this->sourceFields['newmsg'] = t('Boolean, if the message has been read by the user');
    $this->sourceFields['hostname'] = t('Hostname of the sender');
    $this->sourceFields['folder'] = t('The folder into which the message has been filed');
    $this->sourceFields['author_del'] = t('Boolean, if the author has deleted the message');
    $this->sourceFields['recipient_del'] = t('Boolean, if the recipient has deleted the message');
    $this->sourceFields['format'] = t('The text format of the body text');

    $this->source = new MigrateSourceSQL($query, $this->sourceFields, NULL, $this->sourceOptions);
    $this->destination = new MigrateDestinationPrivatemsg7CRUD();
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'Source message ID',
          'alias' => 'p',
        ),
      ),
      MigrateDestinationPrivatemsg7CRUD::getKeySchema()
    );

    $this->highwaterField = array(
      'name' => 'id',
      'alias' => 'p',
      'type' => 'int',
    );

    // Setup common mappings.
    $this->addSimpleMappings(array(
      'subject',
      'timestamp',
    ));

    $this->addFieldMapping('author', 'author')
      ->sourceMigration($user_migration);
    $this->addFieldMapping('recipients', 'recipient')
      ->sourceMigration($user_migration);

    $this->addFieldMapping('body', 'message');
    $this->addFieldMapping('format', 'format')
      ->callbacks(array($this, 'mapFormat'));

    $this->addFieldMapping('is_new', 'newmsg');

    $this->addUnmigratedSources(array(
      'hostname',
      'folder',
    ));
  }

  /**
   * Implements Migration::prepareRow().
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    if (is_numeric($row->recipient)) {
      $row->recipients = array($row->recipient);
    }
    if (is_array($row->recipient)) {
      $row->recipients = $row->recipient;
    }
  }

  /**
   * Implements Migration::prepare().
   */
  public function prepare($entity, stdClass $row) {
    // See if we can figure out which thread this message belongs to.
    $pattern = "/^Re: /i";
    if (preg_match($pattern, $entity->subject)) {
      $orig_title = trim(preg_replace($pattern, '', $entity->subject));
      $entity->subject = $orig_title;
    }
    $args = array(
      ':subject' => $entity->subject,
      ':author' => $entity->author,
      ':recipient' => $entity->recipients,
    );
    $sql = "SELECT m.mid, i.thread_id
            FROM {pm_index} i
            LEFT JOIN {pm_message} m ON i.thread_id = m.mid
            WHERE
              m.subject = :subject AND
              (m.author = :author OR i.recipient = :recipient)
            ORDER BY i.thread_id ASC
            LIMIT 0,1";
    $result = db_query($sql, $args)->fetchObject();
    if (!empty($result) && !empty($result->thread_id)) {
      $entity->thread_id = $result->thread_id;
    }
  }

  /**
   * Implements Migration::complete().
   */
  public function complete($entity, stdClass $row) {
    // Set the unread status, if unread
    if ($row->newmsg == PRIVATEMSG_UNREAD) {
      foreach ($entity->message->recipients as $account) {
        privatemsg_thread_change_status($entity->message->mid, PRIVATEMSG_UNREAD, $account);
      }
    }
    // Save the deleted state for sender and recipient.
    if ($row->author_del) {
      privatemsg_message_change_delete($entity->message->mid, 1, $entity->message->author);
    }
    if ($row->recipient_del) {
      foreach ($entity->message->recipients as $account) {
        privatemsg_message_change_delete($entity->message->mid, 1, $account);
      }
    }
  }

  /**
   * Implements Migration::query().
   *
   * Query for basic private message fields from Drupal 5.
   *
   * @return QueryConditionInterface
   */
  protected function query() {
    $query = Database::getConnection('default', $this->sourceConnection)
      ->select('privatemsg', 'p')
      ->fields('p', array(
        'id', 'author', 'recipient',
        'subject', 'message', 'timestamp', 'newmsg',
        'hostname', 'folder', 'author_del', 'recipient_del', 'format',
      ))
      ->orderBy('id');
    return $query;
  }

}
