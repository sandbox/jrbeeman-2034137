Migrate module support for Privatemsg.

Note: Requires "Full public CRUD API" support for Privatemsg. Patch can be found
in https://drupal.org/node/1184984